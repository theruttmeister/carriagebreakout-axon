Carriage distribution breakout PCB 
----------------------------------
Designed for Series 1 2014 Model, using geda:gaf 

This distribution pcb connects signals from the gantry pcb distirbution board, and connects the hot ends, fans, thermistors, aux, and extruder motors.

Refer to semver.org for versioning information. As far as I know, symantic versioning hasn't been used for HW before, dependancies will be added as they become clear in the master overview of the systems integrated with this board. 
